module.exports = {
  keys: require('./keys'),
  passportConfig: require('./passport')
}