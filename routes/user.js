const passport = require('passport');
const router = require('express').Router();

const {
  registerUser,
  authenticateUser,
  getUsers,
  getAllUsers,
  getUserDetailsById
} = require('./components');

// @route   POST api/u/
// @desc    Get complete list of users
// @access  Public
router.get('/', getUsers);

// @route   GET api/u/testwatch
// @desc    Test api if it is working
// @access  Public
router.get('/testwatch', (req, res) => {
  res.status(200).json({ msg: 'User route is working'});
});

// @route   POST api/u/register
// @desc    Register a new user
// @access  Public
router.post('/register', registerUser);

// @route   POST api/u/authenticate
// @desc    Authenticate a user
// @access  Public
router.post('/authenticate', authenticateUser);

// @route   GET api/u/all
// @desc    Get complete list of users in minified version
// @access  Private
router.get('/all', passport.authenticate("jwt", { session: false }), getAllUsers);

// @route   GET api/u/:id
// @desc    Get user details by id
// @access  Private
router.get('/:id', passport.authenticate("jwt", { session: false }), getUserDetailsById);

module.exports = router;