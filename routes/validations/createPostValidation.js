const validator = require('validator');
const _ = require('lodash');

const createPostValidation = data => {
  let errors = {};

  data.title = !_.isEmpty(data.title) ? data.title : "";
  data.description = !_.isEmpty(data.description) ? data.description : "";
  data.tags = !_.isEmpty(data.tags) ? data.tags : "";

  if(!validator.isLength(data.title, { min: 5, max: 200 })) {
    errors.title = 'Title must be between 5 to 200 characters';
  }
  if(validator.isEmpty(data.title)) {
    errors.title = 'Title is required';
  }

  if(!validator.isLength(data.description, { min: 10, max: 300 })) {
    errors.description = 'Description must be between 10 to 300 characters';
  }
  if(validator.isEmpty(data.description)) {
    errors.description = 'Description is required';
  }

  if(validator.isEmpty(data.tags)) {
    errors.tags = 'Provide at least one tag';
  }
  
  return {
    errors,
    isValid: _.isEmpty(errors)
  };
};

module.exports = createPostValidation;