module.exports = {
  registerUserValidation: require('./registerUserValidation'),
  authenticateUserValidation: require('./authenticateUserValidation'),

  createPostValidation: require('./createPostValidation'),
  editPostValidation: require('./editPostValidation'),

  createCommunityValidation: require('./createCommunityValidation'),
};