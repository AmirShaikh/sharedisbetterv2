const validator = require('validator');
const _ = require('lodash');

const { passwordValidations } = require('../../utils').validations;

const registerUserValidation = data => {
  let errors = {};

  data.username = !_.isEmpty(data.username) ? data.username : "";
  data.email = !_.isEmpty(data.email) ? data.email : "";
  data.password = !_.isEmpty(data.password) ? data.password : "";
  data.confirmPassword = !_.isEmpty(data.confirmPassword)
    ? data.confirmPassword
    : "";

  if(!validator.isLength(data.username, { min: 2, max: 30 })) {
    errors.username = 'Username must be between 2 to 30 characters';
  }
  if(validator.isEmpty(data.username)) {
    errors.username = 'Username is required';
  }
  
  if(!validator.isEmail(data.email)) {
    errors.email = 'Email invalid';
  }
  if(validator.isEmpty(data.email)) {
    errors.email = 'Email is required';
  }

  const passwordErrors = passwordValidations.validatedPassword(data.password);
  if(!passwordErrors.isValid) {
    errors.passwordChecks = passwordErrors.errors;
  }
  if(validator.isEmpty(data.password)) {
    errors.password = 'Password is required';
  }

  if (!validator.equals(data.password, data.confirmPassword)) {
    errors.confirmPassword = 'Passwords must match';
  }

  if (validator.isEmpty(data.confirmPassword)) {
    errors.confirmPassword = 'Confirm password is required';
  }
  
  return {
    errors,
    isValid: _.isEmpty(errors)
  };
};

module.exports = registerUserValidation;