const validator = require('validator');
const _ = require('lodash');

const authenticateUserValidation = data => {
  let errors = {};

  data.username = !_.isEmpty(data.username) ? data.username : "";
  data.password = !_.isEmpty(data.password) ? data.password : "";

  if(validator.isEmpty(data.username)) {
    errors.username = 'Username is required';
  }

  if(validator.isEmpty(data.password)) {
    errors.password = 'Password is required';
  }
  
  return {
    errors,
    isValid: _.isEmpty(errors)
  };
};

module.exports = authenticateUserValidation;