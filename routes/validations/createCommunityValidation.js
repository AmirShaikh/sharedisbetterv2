const validator = require('validator');
const _ = require('lodash');

module.exports = data => {
  let errors = {};

  data.title = !_.isEmpty(data.title) ? data.title : "";
  data.bio = !_.isEmpty(data.bio) ? data.bio : "";

  if(!validator.isLength(data.title, { min: 5, max: 200 })) {
    errors.title = 'Title must be between 5 to 200 characters';
  }
  if(validator.isEmpty(data.title)) {
    errors.title = 'Title is required';
  }

  if(!validator.isLength(data.bio, { min: 5, max: 300 })) {
    errors.bio = 'Bio must be between 5 to 300 characters';
  }
  if(validator.isEmpty(data.bio)) {
    errors.bio = 'Bio is required';
  }
  
  return {
    errors,
    isValid: _.isEmpty(errors)
  };
};