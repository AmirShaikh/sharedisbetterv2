const mongoose = require('mongoose');
const _ = require('lodash');

const { Post, Community } = require('../models');

module.exports = (req, res) => {
  const communities = _.split(req.body.communities, ',');

  Community
    .find({ _id: { $in: communities } } )
    .then(lists => {
      if(!_.reduce(lists, item => _.some(item.members, ['member', req.user._id]))){
        return res.status(400).json({ message: 'User is not a member/admin' });
      }
      // TODO: Check if post is already shared with the community
      Post
        .findOneAndUpdate(
          { author: req.user._id, _id: req.params.id },
          { $push: { communities } },
          { new: true, upsert: true }
        )
        .then(post => {
          Community.updateMany(
            { _id: { $in: post.communities }},
            { $push: { posts: {post: req.params.id } } },
            { new: true, upsert: true }
          )
          .then(community => res.json(post))
          .catch(error => res.status(400).json(error));
        })
        .catch(error => res.status(400).json(error));
    })
    .catch(error => res.status(400).json({ message: 'Share post with community failed', error }));
};