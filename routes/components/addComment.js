const _ = require('lodash');

const { Post } = require('../models');

module.exports = (req, res) => {
  const findPostQuery = {
    _id: req.params.postId
  }

  Post
    .findOne(findPostQuery)
    .then(post => {
      if(_.isEmpty(post)){
        return res.status(400).json({ message: 'Invalid post not found' });
      }
      if(_.isEqual(post.author, req.user._id)){
        return res.status(400).json({ message: 'Author can only response to comment'});
      }
      if(_.some(post.comments, ['commentedBy', req.user._id])){
        return res.status(400).json({ message: 'Comment can be added for once'});
      }

      const pushCommentQuery = {
        $push: {
          comments: {
            commentedBy: req.user._id,
            comment: req.body.comment
          }
        }
      }

      Post
        .findOneAndUpdate(
          findPostQuery,
          pushCommentQuery,
          { new: true, upsert: true }
        )
        .then(post => res.json({ message: 'Comment added to the post', post }))
        .catch(error => res.status(400).json({ message: 'Add comment to post failed ', error }));
    })
    .catch(error => res.status(400).json({ message: 'Add comment to post failed ', error }));
};