const mongoose = require('mongoose');
const _ = require('lodash');

const { User, Community } = require('../models');

module.exports = (req, res) => {
  User
    .findById(req.params.id)
    .populate('communities.community', '_id handle title location')
    .then(user => {
      if(_.isEmpty(user)){
        return res.status(400).json({ message: 'User not found' });
      }
      if(_.isEmpty(user.communities)) {
        return res.status(400).json({ message: 'User has no communities' });
      }
      return res.json(user.communities);
    })
    .catch(error => console.log(error));
};