const mongoose = require('mongoose');
const _ = require('lodash');

const { Community } = require('../models');
const { adminLookup } = require('../lookups').communityLookupsUser;

module.exports = (req, res) => {
  Community
    .aggregate([
      { $match: { _id: new mongoose.Types.ObjectId(req.params.id) } },
      { $lookup: adminLookup }
    ])
    .then(community => {
      if(_.isEmpty(community)){
        return res.status(400).json({ message: 'No community found' });
      }
      return res.json(community[0]);
    })
    .catch(error => res.status(400).json(error));
};