const _ = require('lodash');
const mongoose = require('mongoose');

const { User } = require('../models');
const { userDefaults } = require('../../utils').defaults;
const { computeOrder, computeLimit } = require('../helpers');
const { userProjects } = require('../projects');

module.exports = (req, res) => {
  const order = computeOrder(req.query.order || userDefaults.order);
  const limit = computeLimit(req.query.limit || userDefaults.limit) + 1;

  User.aggregate([
    { $sort: order },
    { $limit: limit },
    { $match: {
      _id: { $ne: new mongoose.Types.ObjectId(req.user._id) }
    }},
    { $project: userProjects.minified }
  ])
    .then(users => {
      console.log(users);
      if(_.isEmpty(users)){
        return res.status(400).json({ message: 'No user found' });
      }
      return res.json(users);
    })
    .catch(error => res.status(404).json({ user: 'Get all users not working', error }));
};