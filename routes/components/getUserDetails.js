const { User } = require('../models');

const getUserDetails = (req, res) => {
  const { key, value } = req.params;
  const query = {[key]: value};
  User.findOne(query)
    .then(user => {
      if(user){
        return res.json(user);
      }
      return res.status(400).json({ user: 'User details not found' });
    })
};

module.exports = getUserDetails;