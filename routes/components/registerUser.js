const gravatar = require('gravatar');
const bcryptJs = require('bcryptjs');
const _ = require('lodash');

const { User } = require('../models');
const { registerUserValidation } = require('../validations');

module.exports = (req, res) => {
  const { errors, isValid } = registerUserValidation(req.body);

  if (!isValid) {
    return res.status(400).json(errors);
  }

  const { username, email, password } = req.body;

  User.findOne({ username })
    .then(user => {
      if(!_.isEmpty(user)) {
        errors.username = 'Username already exist';
        return res.status(400).json(errors);
      }
      
      User.findOne({ email })
        .then(user => {
          if(!_.isEmpty(user)) {
            errors.email = 'Email already exist';
            return res.status(400).json(errors);
          }

          const avatar = gravatar.url(email, { s: "200", r: "pg", d: "mm" });
          const newUser = new User({ username, email, password, avatar });

          bcryptJs.genSalt(10, (err, salt) => 
            bcryptJs.hash(password, salt, (err, hash) => {
              if(err) throw err;
              newUser.password = hash;

              newUser.save()
                .then(user => res.json(user))
                .catch(err => res.status(400).json({ msg: 'User creation failed ', err }))
            })
          );
        });
    });
};