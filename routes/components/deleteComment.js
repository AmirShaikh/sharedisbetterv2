const mongoose = require('mongoose');
const _ = require('lodash');

const { Post } = require('../models');

module.exports = (req, res) => {
  const postFindQuery = {
    _id: req.params.postId,
    'comments._id': req.params.commentId,
    'comments.commentedBy': req.user._id
  };

  Post
    .findOne(postFindQuery)
    .then(post => {
      if(_.isEmpty(post)){
        return res.status(400).json({ message: 'Invalid post not found' });
      }
      if(!_.some(post.comments, ['_id', new mongoose.Types.ObjectId(req.params.commentId)])){
        return res.status(400).json({ message: 'Comment not found'});
      }

      const deleteCommentQuery = {
        $pull: { 
          comments: { 
            $elemMatch: { 
              _id: req.params.commentId
            }
          }
        }
      };

      Post
        .findOneAndUpdate(
          postFindQuery,
          deleteCommentQuery,
          { new: true, upsert: true }
        )
        .then(post => res.json({ message: 'Comment deleted', post }))
        .catch(error => res.status(400).json({ message: 'Delete comment to post failed ', error }));
    })
    .catch(error => res.status(400).json({ message: 'Delete comment to post failed ', error }));
};