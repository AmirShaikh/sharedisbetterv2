const mongoose = require('mongoose');
const _ = require('lodash');

const { Community, User, Post } = require('../models');
const { createPostValidation } = require('../validations');

const createPost = (req, res) => {
  const { errors, isValid } = createPostValidation(req.body);

  if(!isValid) {
    return res.status(400).json(errors);
  }

  const { title, description, tags, communities } = req.body;
  const newPost = new Post({
    author: req.user._id,
    title,
    description,
    tags: _.split(tags, ','),
    communities: _.split(communities, ',')
  });

  Community
    .find({ _id: { $in: newPost.communities } } )
    .then(lists => {
      if(!_.reduce(lists, item => _.some(item.members, ['member', req.user._id]))){
        return res.status(400).json({ message: 'User is not a member/admin' });
      }
      newPost
        .save()
        .then(post => {
          User
            .findOneAndUpdate(
              { _id: new mongoose.Types.ObjectId(req.user._id)},
              { $push: { posts: { post: post._id } } },
              { new: true, upsert: true }
            )
            .then(user => {
              Community
                .updateMany(
                  { _id: { $in: post.communities }},
                  { $push: { posts: {post: post._id } } },
                  { new: true, upsert: true }
                )
                .then(community => res.json(post))
                .catch(error => res.status(400).json(error));
            })
            .catch(error => res.status(400).json(error));
        })
        .catch(error => res.status(400).json(error));
    })
    .catch(error => res.status(400).json(error));
};

module.exports = createPost;