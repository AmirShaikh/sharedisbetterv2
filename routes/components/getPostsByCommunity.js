const mongoose = require('mongoose');
const _ = require('lodash');

const { Post } = require('../models');
const { postDefaults } = require('../../utils').defaults;
const { computeOrder, computeLimit } = require('../helpers');
const lookups = require('../lookups');
const { postProjects } = require('../projects');

module.exports = (req, res) => {
  const limit = computeLimit(req.query.limit || postDefaults.limit);
  const order = computeOrder(req.query.order || postDefaults.order);

  Post
    .aggregate([
      { $match: {
        communities: new mongoose.Types.ObjectId(req.params.id)
      }},
      { $sort: order },
      { $limit: limit },
      { $lookup: lookups.userLookup },
      { $lookup: lookups.postLookupsCommunity.communityLookup },
      { $project: postProjects.minifiedForPostsByCommunity }
    ])
    .then(posts => {
      console.log(posts);
      if(_.isEmpty(posts)){
        return res.status(400).json({ message: 'No post found' });
      }
      return res.json(posts);
    })
    .catch(error => res.status(400).json({ getpostsbyuser: 'failed' , error }));
};