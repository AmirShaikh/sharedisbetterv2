const mongoose = require('mongoose');

const { User, Community } = require('../models');
const { createCommunityValidation } = require('../validations');

module.exports = (req, res) => {
  const { errors, isValid } = createCommunityValidation(req.body);

  if(!isValid){
    return res.status(400).json(errors);
  }

  const { title, bio, handle, location } = req.body;
  const newCommunity = new Community({
    admin: req.user._id,
    title,
    bio,
    handle,
    location,
    members: { member: req.user._id }
  });
  
  newCommunity
    .save()
    .then(community => {
      User
        .findOneAndUpdate(
          { _id: new mongoose.Types.ObjectId(req.user._id)},
          { $push: { communities: { community: community._id, role: 'admin' } } },
          { new: true, upsert: true }
        )
        .then(user => res.json(community))
        .catch(error => res.status(400).json(error));
    })
    .catch(error => res.status(400).json(error));
};