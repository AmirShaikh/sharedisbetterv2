const mongoose = require('mongoose');
const _ = require('lodash');

const { Post } = require('../models');
const { editPostValidation } = require('../validations');

module.exports = (req, res) => {
  const { errors, isValid } = editPostValidation(req.body);

  if(!isValid){
    return res.status(400).json(errors);
  }

  const { title, description, tags } = req.body;
  Post
    .findOneAndUpdate(
      { author: req.user._id, _id: req.params.id },
      {
        title,
        description,
        tags: _.split(tags, ','),
        updatedAt: Date.now
      },
      { new: true, upsert: true }
    )
    .then(post => {
      if(_.isEmpty(post)){
        res.status(400).json({ message: 'Post not found' });
      }
      res.json(post);
    })
    .catch(error => res.status(400).json({ message: 'Edit post is not working', error }));
};