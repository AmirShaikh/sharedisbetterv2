const _ = require('lodash');

const { Post } = require('../models');

module.exports = (req, res) => {
  Post
    .findOneAndDelete({ author: req.user._id, _id: req.params.id })
    .then(post => {
      if(_.isEmpty(post)){
        res.status(400).json({ message: 'Post not found' });
      }
      res.json({ message: 'Post is deleted'});
    })
    .catch(error => res.status(400).json({ message: 'Delete post is not working', error }));
};