const mongoose = require('mongoose');
const _ = require('lodash');

const { Community, User } = require('../models');

module.exports = (req, res) => {
  Community
    .findById(req.params.id)
    .then(community => {
      if(_.isEmpty(community)){
        return res.status(400).json({ message: 'Invalid community id' });
      }
      if(_.isEqual(community.admin, req.user._id)){
        return res.status(400).json({ message: 'Transfer adminship and then exit' });
      }
      if(_.some(community.members, ['member', req.user._id])){
        Community
          .findOneAndUpdate(
            { _id: new mongoose.Types.ObjectId(req.params.id)},
            { $pull: { members: { member: req.user._id } } },
            { new: true, upsert: true }
          )
          .then(community => {
            User
              .findOneAndUpdate(
                { _id: new mongoose.Types.ObjectId(req.user._id)},
                { $pull: { communities: { community: req.params.id } } },
                { new: true, upsert: true }
              )
              .then(user => res.json(community))
              .catch(error => res.status(400).json(error));
          })
          .catch(error => res.status(400).json(error));
      }
    })
    .catch(error => res.status(400).json(error));
};