const mongoose = require('mongoose');
const _ = require('lodash');

const { Post } = require('../models');
const lookups = require('../lookups');

const getPostDetails = (req, res) => {
  Post
    .aggregate([
      { $match: { _id: new mongoose.Types.ObjectId(req.params.id) } },
      { $lookup: lookups.userLookup },
      { $lookup: lookups.postLookupsCommunity.communityLookup }
    ])
    .then(post => {
      console.log(post);
      if(_.isEmpty(post)){
        return res.status(400).json({ message: 'Post not found'});
      }
      return res.json(post[0]);
    })
    .catch(error => res.status(400).json(error));
};

module.exports = getPostDetails;