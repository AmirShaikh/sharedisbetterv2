const _ = require('lodash');
const { Post } = require('../models');

const getPosts = (req, res) => {
  Post.find()
    .populate('author', 'username avatar')
    .populate('communities', 'title handle location')
    .populate('votes.votedBy', 'username avatar')
    .populate('comments.commentedBy', 'username avatar')
    .populate('comments.response.respondedBy', 'username avatar')
    .then(posts => {
      if(_.isEmpty(posts)){
        return res.status(400).json({ message: 'No post found' });
      }
      return res.json({posts, records: posts.length});
    })
    .catch(error => res.status(400).json(error));
};

module.exports = getPosts;