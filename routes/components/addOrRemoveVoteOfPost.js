const _ = require('lodash');

const { Post } = require('../models');

module.exports = (req, res) => {
  Post
    .findById(req.params.id)
    .then(post => {
      const shouldUpdate = req.query.voted === 'true';
      const { _id: userId, username } = req.user;
      
      if(_.isEmpty(post)){
        return res.status(400).json({ message: 'Invalid post not found' });
      }
      if(_.some(post.votes, ['votedBy', userId]) && shouldUpdate){
        return res.status(400).json({ message: `Already up voted this post by ${username}` });
      }
      if(!_.some(post.votes, ['votedBy', userId]) && !shouldUpdate){
        return res.status(400).json({ message: `Can'be down voted by ${username}` });
      }

      const pushQuery = { $push: { votes: { votedBy: userId } } };
      const pullQuery = { $pull: { votes: { votedBy: userId } } };
      
      Post.findByIdAndUpdate(
        { _id: req.params.id },
        shouldUpdate ? pushQuery : pullQuery,
        { new: true, upsert: true }
      )
      .then(post => res.json({ message: `Post is ${shouldUpdate ? 'up voted' : 'down voted'}`, post }))
      .catch(error => res.status(400).json({ message: 'Post voting failed', error }));
    })
    .catch(error => res.status(400).json({ message: 'Post voting failed', error }));
};