const _ = require('lodash');

const { Community } = require('../models');
const { communityDefaults } = require('../../utils').defaults;
const { computeLimit, computeOrder } = require('../helpers');
const { adminLookup } = require('../lookups').communityLookupsUser;
const { communityMinified } = require('../projects');

module.exports = (req, res) => {
  const limit = computeLimit(req.query.limit || communityDefaults.limit);
  const order = computeOrder(req.query.order || communityDefaults.order);

  Community
    .aggregate([
      { $sort: order },
      { $limit: limit },
      { $lookup: adminLookup },
      { $project: communityMinified }
    ])
    .then(communities => {
      if(_.isEmpty(communities)){
        return res.status(400).json({ message: 'No community found' });
      }
      return res.json(communities);
    })
    .catch(error => res.status(400).json(error));
};