const mongoose = require('mongoose');
const _ = require('lodash');

const { Post } = require('../models');

module.exports = (req, res) => {
  const postFindQuery = {
    '_id': req.params.postId,
    'author': req.user._id,
    'comments._id': req.params.commentId
  };

  Post
    .findOne(postFindQuery)
    .then(post => {
      if(_.isEmpty(post)){
        return res.status(400).json({ message: 'Unauthorised' });
      }
      const alreadyResponded = _.hasIn(
        _.reduce(post.comments, comment => _.isEqual(comment._id, new mongoose.Types.ObjectId(req.params.commentId)))
        , 'response.respondedAt'
      );

      const shouldUpdate = req.query.edit === 'true';

      if(alreadyResponded && !shouldUpdate){
        return res.status(400).json({ message: 'Already responded' });
      }

      const updateCommentData = {
        response: req.body.response,
        respondedBy: req.user._id
      };
      
      if(alreadyResponded && shouldUpdate){
        updateCommentData.updatedAt = Date.now();
      }

      const updatePostQuery = {
        $set: {
          'comments.$.response': updateCommentData
        }
      };
      
      Post
        .findOneAndUpdate(
          postFindQuery,
          updatePostQuery,
          { new: true, upsert: true}
        )
        .then(post => res.json({ message: 'Comment replied', post }))
        .catch(error => res.status(400).json({ message: 'Reply by author to comment failed', error }));
    })
    .catch(error => res.status(400).json({ message: 'Reply by author to comment failed', error }));
};