const mongoose = require('mongoose');
const _ = require('lodash');

const { User } = require('../models');

module.exports = (req, res) => {
  User
  // .findById(req.params.id)
  //   .populate('communities.community', 'title handle location')  
  .aggregate([
      { $match: { _id: new mongoose.Types.ObjectId(req.params.id) } },
      { $lookup: {
        from: 'community',
        let: { communities: "$communities" },
        pipeline: [
          {
            $match: {
              $expr: {
                $in: ['$_id', '$$communities.community']
              }
            }
          },
          { $project: {
            _id: 1,
            title: 1,
            handle: 1,
            location: 1,
            membersCount: { $size: '$members' },
            postsCount: { $size: '$posts' }
          }}
        ],
        as: 'communities'
      }},
    ])
    .then(user => {
      console.log(user);
      if(_.isEmpty(user)){
        return res.status(400).json({ message: 'User not found '});
      }
      return res.json(user);
    })
    .catch(error => res.status(400).json({ message: 'Get user details not working ', error }));
};