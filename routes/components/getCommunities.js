const _ = require('lodash');

const { Community } = require('../models');

module.exports = (req, res) => {
  Community
    .find()
    .populate('admin', 'username avatar')
    .then(communities => {
      if(_.isEmpty(communities)){
        return res.status(400).json({ message: 'No community found' });
      }
      return res.json(communities);
    })
    .catch(error => res.status(400).json(error));
};