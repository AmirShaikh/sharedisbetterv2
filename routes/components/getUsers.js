const _ = require('lodash');

const { User } = require('../models');

module.exports = (req, res) => {
  User
    .find()
    .populate('communities.community', 'title handle location')
    .populate('posts.post', 'title')
    .then(users => {
      if(_.isEmpty(users)){
        res.status(400).json({ message: 'No user found' });
      }
      res.json({ records: users.length, users });
    })
    .catch(error => res.status(400).json(error));
};