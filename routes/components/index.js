module.exports = {
  registerUser: require('./registerUser'),
  authenticateUser: require('./authenticateUser'),
  getUsers: require('./getUsers'),
  getAllUsers: require('./getAllUsers'),
  getUserDetailsById: require('./getUserDetailsById'),

  createPost: require('./createPost'),
  editPost: require('./editPost'),
  deletePost: require('./deletePost'),

  getPosts: require('./getPosts'),
  getPostsByUser: require('./getPostsByuser'),
  getPostsByCommunity: require('./getPostsByCommunity'),
  getPostDetails: require('./getPostDetails'),

  sharePostWithCommunity: require('./sharePostWithCommunity'),
  addOrRemoveVoteOfPost: require('./addOrRemoveVoteOfPost'),

  addComment: require('./addComment'),
  deleteComment: require('./deleteComment'),
  replyByAuthorToComment: require('./replyByAuthorToComment'),

  createCommunity: require('./createCommunity'),
  getCommunities: require('./getCommunities'),
  getAllCommunities: require('./getAllCommunities'),
  getCommunityById: require('./getCommunityById'),
  getCommunityByHandle: require('./getCommunityByHandle'),
  
  getCommunitiesOfUser: require('./getCommunitiesOfUser'),
  joinCommunity: require('./joinCommunity'),
  exitCommunity: require('./exitCommunity'),
};