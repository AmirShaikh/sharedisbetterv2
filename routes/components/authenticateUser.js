const _ = require('lodash');
const bcryptJs = require('bcryptjs');
const jwt = require('jsonwebtoken');

const { User } = require('../models');
const { keys } = require('../../config');
const { authenticateUserValidation } = require('../validations');

module.exports = (req, res) => {
  const { errors, isValid } = authenticateUserValidation(req.body);

  if(!isValid){
    return res.status(400).json(errors);
  }
  const { username, password } = req.body;

  User.findOne({ username })
    .select('+password')
    .then(user => {
      if(_.isEmpty(user)) { 
        errors.username = 'Invalid username';
        return res.status(400).json(errors);
      } else {
        bcryptJs.compare(password, user.password)
          .then(isMatched => {
            if(!isMatched) {
              errors.password = 'Incorrect password';
              return res.status(400).json(errors);
            } else {
              const payload = {
                _id: user._id,
                username: user.username,
                email: user.email,
                avatar: user.avatar
              };

              jwt.sign(payload, keys.secretOrKey, { expiresIn: 3600 }, 
                (err, token) => {
                  payload.token = `Bearer ${token}`;
                  return res.json(payload);
                });
            }
          })
      }
    });
};