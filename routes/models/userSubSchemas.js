const schema = require('mongoose').Schema;

const userCommunities = new schema({
  community: { type: schema.Types.ObjectId, ref: 'community' },
  role: { type: String, enum: ['admin','member'], default: 'member' }
}, { _id: false });

const userPosts = new schema({
  post: { type: schema.Types.ObjectId, ref: 'post' }
}, { _id: false });

module.exports = {
  userCommunities,
  userPosts
};