const mongoose = require('mongoose');
const schema = mongoose.Schema;

const { userCommunities, userPosts } = require('./userSubSchemas');

const UserSchema = new schema({
  username: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
    select: false
  },
  avatar: String,
  name: String,
  bio: String,
  communities: [ userCommunities ],
  posts: [ userPosts ],
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: Date
});

module.exports = User = mongoose.model('user', UserSchema, 'user');