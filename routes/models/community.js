const mongoose = require('mongoose');
const schema = mongoose.Schema;

const CommunitySchema = new schema({
  admin: {
    type: schema.Types.ObjectId,
    ref: 'user'
  },
  title: {
    type: String,
    required: true,
    min: 5,
    max: 200
  },
  bio: {
    type: String,
    min: 5,
    max: 300,
    required: true
  },
  handle: {
    type: String,
    max: 30,
    unique: true
  },
  location: String,
  members: [{ 
    member: { type: schema.Types.ObjectId, ref:'user' },
    joinedAt: { type: Date, default: Date.now() }
  }],
  posts: [{
    post:{ type: schema.Types.ObjectId, ref: 'post' },
    sharedAt: { type: Date, default: Date.now() }
  }],
  createdAt: { type: Date, default: Date.now() },
  updatedAt: Date
});

module.exports = Community = mongoose.model('community', CommunitySchema, 'community');