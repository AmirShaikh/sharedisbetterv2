const mongoose = require('mongoose');
const schema = mongoose.Schema;

const { postVotes, postComments, postCommunities } = require('./postSubSchemas')

const PostSchema = new schema({
  author: {
    type: schema.Types.ObjectId,
    ref: 'user'
  },
  title: {
    type: String,
    required: true,
    min: 5,
    max: 200,
    trim: true
  },
  description: {
    type: String,
    required: true,
    min: 10,
    max: 300,
    trim: true
  },
  tags: [{
    type: String,
    required: true,
    trim: true
  }],
  communities: [{ type: schema.Types.ObjectId, ref: 'community' }],
  votes: [ postVotes ],
  comments: [ postComments ],
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: Date
});

module.exports = Post = mongoose.model('post', PostSchema, 'post');