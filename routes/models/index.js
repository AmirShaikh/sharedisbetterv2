module.exports = {
  Community: require('./community'),
  Post: require('./post'),
  User: require('./user')
};