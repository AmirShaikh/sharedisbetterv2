const schema = require('mongoose').Schema;

const postVotes = new schema({
  votedBy: { type: schema.Types.ObjectId, ref: 'user' },
  votedAt: { type: Date, default: Date.now }
}, { _id: false });

const postCommentReply = new schema({
  respondedBy: { type: schema.Types.ObjectId, ref: 'user' },
  respondedAt: { type: Date, default: Date.now },
  response: { type: String, trim: true },
  updatedAt: Date
}, { _id: false });

const postComments = new schema({
  commentedBy: { type: schema.Types.ObjectId, ref: 'user' },
  commentedAt: { type: Date, default: Date.now },
  comment: { type: String, trim: true },
  updatedAt: Date,
  response: postCommentReply
});

const postCommunities = new schema({
  community: { type: schema.Types.ObjectId, ref: 'Community' },
  addedAt: { type: Date, default: Date.now }
}, { _id: false });

module.exports = {
  postVotes,
  postComments,
  postCommunities
};