const router = require('express').Router();
const passport = require('passport');

const { 
  createPost,
  getPosts,
  getPostsByUser,
  getPostsByCommunity,
  getPostDetails,
  editPost,
  deletePost,
  sharePostWithCommunity,
  addOrRemoveVoteOfPost,
  addComment,
  deleteComment,
  replyByAuthorToComment,
} = require('./components');

// @route   GET api/p/
// @desc    Get all posts
// @access  Public
router.get('/', getPosts);

// @route   GET api/p/testwatch
// @desc    Test api if it is working
// @access  Public
router.get('/testwatch', (req, res) => {
  res.status(200).json('Post route is working');
});

// @route   POST api/p/create
// @desc    Create new post
// @access  Private
router.post('/create', passport.authenticate('jwt', { session: false }), createPost);

// @route   GET api/p/:id
// @desc    Get post details
// @access  Private
router.get('/:id', passport.authenticate('jwt', { session: false }), getPostDetails);

// @route   DELETE api/p/delete/:id
// @desc    Delete a post
// @access  Private
router.delete('/:id', passport.authenticate('jwt', { session: false }), deletePost);

// @route   PUT api/p/:id
// @desc    Edit a post
// @access  Private
router.put('/:id', passport.authenticate('jwt', { session: false }), editPost);

// @route   GET api/p/byUser/:id
// @desc    Get all posts with minified details with ref to user
// @access  Private
router.get('/byUser/:id', passport.authenticate('jwt', { session: false }), getPostsByUser);

// @route   GET api/p/byCommunity/:id
// @desc    Get all posts with minified details with ref to community
// @access  Private
router.get('/byCommunity/:id', passport.authenticate('jwt', { session: false }), getPostsByCommunity);

// @route   PUT api/p/shareWithCommunity/:id
// @desc    Share post with community
// @access  Private
router.put('/shareWithCommunity/:id', passport.authenticate('jwt', { session: false }), sharePostWithCommunity);

// @route   PUT api/p/vote/:id?upVoted=true/false
// @desc    Add/Remove a vote to a post
// @access  Private
router.put('/vote/:id', passport.authenticate('jwt', { session: false }), addOrRemoveVoteOfPost);

// @route   PUT api/p/comment/:postId
// @desc    Add a comment
// @access  Private
router.put('/comment/:postId', passport.authenticate('jwt', { session: false }), addComment);

// @route   DELETE api/p/comment/:postId/:commentId
// @desc    Delete a comment
// @access  Private
router.delete('/comment/:postId/:commentId', passport.authenticate('jwt', { session: false }), deleteComment);

// @route   PUT api/p/comment/replyByAuthor/:postId/:commentId?edit=true/false
// @desc    Author reply to a comment add and edit
// @access  Private
router.put('/comment/replyByAuthor/:postId/:commentId', passport.authenticate('jwt', { session: false }), replyByAuthorToComment);

module.exports = router;