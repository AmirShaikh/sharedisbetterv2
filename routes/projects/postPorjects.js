const minified = {
  _id: 1,
  title: 1,
  description: 1,
  createdAt: 1,
  updatedAt: 1,
  author: 1,
  communities: 1,
  communitiesCount: { $size: '$communities' },
  likesCount: { $size: '$likes' },
  commentsCount: { $size: '$comments' }
};

const minifiedForPostsByCommunity = {
  _id: 1,
  title: 1,
  description: 1,
  createdAt: 1,
  updatedAt: 1,
  author: 1,
  communities: 1,
  communitiesCount: { $size: '$communities.community' },
  likesCount: { $size: '$likes' },
  commentsCount: { $size: '$comments' }
}

module.exports = {
  minified,
  minifiedForPostsByCommunity
};