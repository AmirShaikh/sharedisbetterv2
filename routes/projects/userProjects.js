const minified = {
  _id: 1,
  username: 1,
  email: 1,
  avatar: 1,
  communitiesCount: { $size: '$communities' },
  postsCount: { $size: '$posts' }
}

module.exports = {
  minified
};