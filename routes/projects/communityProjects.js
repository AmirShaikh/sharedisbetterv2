const minified = {
  _id: 1,
  title: 1,
  bio: 1,
  createdAt: 1,
  updatedAt: 1,
  location: 1,
  admin: 1,
  handle: 1,
  membersCount: { $size: '$members' },
  postsCount: { $size: '$posts' }
};

const minifiedForPost = {
  _id: 1,
  title: 1,
  location: 1,
  handle: 1,
  membersCount: { $size: '$members' },
  postsCount: { $size: '$posts' }
};

const minifiedForUser = {
  _id: 1,
  title: 1,
  location: 1,
  handle: 1,
  membersCount: { $size: '$members' },
  postsCount: { $size: '$posts' }
};

module.exports = {
  minified,
  minifiedForPost,
  minifiedForUser
};