const computeLimit = limit => parseInt(limit);

module.exports = computeLimit;