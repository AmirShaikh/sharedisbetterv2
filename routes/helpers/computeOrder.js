const computeOrder = order => {
  let computedOrder = {};
  order
    .split(",")
    .map(
      data =>
        (computedOrder[data.replace("-", "")] = data.startsWith("-") ? -1 : 1)
    );
  return computedOrder;
};

module.exports = computeOrder;