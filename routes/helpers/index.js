module.exports = {
  computeOrder: require('./computeOrder'),
  computeLimit: require('./computeLimit'),
  removePassword: require('./removePassword')
};