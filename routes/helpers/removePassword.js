const removePassword = dataset => {
  dataset.password = undefined;
  dataset = JSON.parse(JSON.stringify(dataset));
  return dataset;
};

module.exports = removePassword;