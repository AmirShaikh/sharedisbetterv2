const { communityProjects } = require('../projects');

const communityLookup = {
  from: 'community',
  let: { communities: "$communities" },
  pipeline: [
    {
      $match: {
        $expr: {
          $in: ['$_id', '$$communities']
        }
      }
    },
    { $project: communityProjects.minifiedForPost }
  ],
  as: 'communities.community'
};

module.exports = {
  communityLookup
};