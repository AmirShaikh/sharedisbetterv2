const userLookup = {
  from: 'user',
  let: { author: "$author" },
  pipeline: [
    {
      $match: {
        $expr: {
          $eq: ['$_id', '$$author']
        }
      }
    },
    { $project: {username: 1, avatar: 1} }
  ],
  as: 'author'
};

module.exports = userLookup;