module.exports = {
  userLookup: require('./userLookup'),

  communityLookupsUser: require('./communityLookupsUser'),

  postLookupsCommunity: require('./postLookupsCommunity'),

  userLookupsCommunity: require('./userLookupsCommunity'),
};