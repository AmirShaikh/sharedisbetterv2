const adminLookup = {
  from: 'user',
  let: { admin: "$admin" },
  pipeline: [
    {
      $match: {
        $expr: {
          $eq: ['$_id', '$$admin']
        }
      }
    },
    { $project: {username: 1, avatar: 1} }
  ],
  as: 'admin'
};

module.exports = {
  adminLookup
};