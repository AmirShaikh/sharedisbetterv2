const { communityProjects } = require('../projects');

const communityLookup = {
  from: 'community',
  let: { communities: "$communities.community" },
  pipeline: [
    {
      $match: {
        $expr: {
          $in: ['$_id', '$$communities']
        }
      }
    },
    { $project: communityProjects.minifiedForUser }
  ],
  as: 'communities'
};

module.exports = {
  communityLookup
};