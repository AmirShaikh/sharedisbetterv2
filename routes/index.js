var router = require("express").Router();

router.use("/u", require("./user"));
router.use("/p", require("./post"));
router.use("/c", require("./community"));

module.exports = router;