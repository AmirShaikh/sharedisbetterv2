const router = require('express').Router();
const passport = require('passport');

const { 
  createCommunity,
  getCommunities,
  getAllCommunities,
  getCommunityById,
  getCommunityByHandle,
  getCommunitiesOfUser,
  joinCommunity,
  exitCommunity
} = require('./components');

// @route   GET api/c/
// @desc    Get all communities
// @access  Public
router
  .get('/', getCommunities);

// @route   GET api/c/testwatch
// @desc    Test api if it is working
// @access  Public
router
  .get('/testwatch', (req, res) => res.status(200).json('Community route is working'));

// @route   POST api/c/create
// @desc    Create new post
// @access  Private
router
  .post('/create', passport.authenticate('jwt', { session: false }), createCommunity);

// @route   GET api/c/all
// @desc    Get all communities
// @access  Private
router
  .get('/all', passport.authenticate('jwt', { session: false }), getAllCommunities);

// @route   GET api/c/byId/:id
// @desc    Get community details with ref to id
// @access  Private
router
  .get('/byId/:id', passport.authenticate('jwt', { session: false }), getCommunityById);

// @route   GET api/c/byHandle/:handle
// @desc    Get community details with ref to handle
// @access  Private
router
  .get('/byHandle/:handle', passport.authenticate('jwt', { session: false }), getCommunityByHandle);

// @route   GET api/c/ofUser/:id
// @desc    Get all communities of user
// @access  Private
router
  .get('/ofUser/:id', passport.authenticate('jwt', { session: false }), getCommunitiesOfUser);

// @route   PUT api/c/join/:id
// @desc    Join a community
// @access  Private
router
  .put('/join/:id', passport.authenticate('jwt', { session: false }), joinCommunity);

// @route   PUT api/c/exit/:id
// @desc    Exit from a community
// @access  Private
router
  .put('/exit/:id', passport.authenticate('jwt', { session: false }), exitCommunity);

module.exports = router;