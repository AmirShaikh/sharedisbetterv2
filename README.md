```npm init```
```
npm i --save 
    bcryptjs
    body-parser
    concurrently
    express
    gravatar
    jsonwebtoken
    mongoose
    nodemon
    passport
    passport-jwt
    validator
    lodash
    password-validator
```
```json
  "scripts": {
      "client-install": "npm install --prefix client",
      "start": "node index.js",
      "server": "nodemon index.js",
      "client": "npm start --prefix client",
      "dev": "concurrently \"npm run server\" \"npm run client\"",
      "test": "karma start --single-run"
    }
```
Git commands
```
git init
git add --all
git commit -m "Initia commit"
git remote add origin https://AmirShaikh@bitbucket.org/AmirShaikh/sharedisbetterv2.git
git push -u origin master
```