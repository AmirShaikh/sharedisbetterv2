import React from 'react';

import { Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import Header from './header';
import Footer from './footer';

const styles = theme => ({
  contentWrapper: {
    maxWidth: 1010
  }
});

const MainLayout = ({ classes, children }) => {
  return(
    <>
      <Header />
      <main>
        <Grid container alignItems="center" className={classes.contentWrapper}>
          {children}
        </Grid>
      </main>
      <Footer />
    </>
  );
}

export default withStyles(styles)(MainLayout);