import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { AppBar, Toolbar, Typography, Grid, IconButton, Badge, InputBase, Menu, MenuItem } from '@material-ui/core';
import { 
  ChatOutlined as ChatIcon,
  NotificationsOutlined as NotificationIcon,
  Search as SearchIcon,
  MoreVertOutlined as MoreIcon,
  AccountCircleOutlined as AccountCircle
} from '@material-ui/icons';

import withRoot from '../../../withRoot';

const logo = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRhbW8vOS9If-qdZ7-4SL30yXffg9sRyryDcil-2I8JoKSp36CKxw';
const styles = theme => ({
  toolbarRoot: {
    width: '100%',
    padding: [[
      theme.spacing.unit * 3.25,
      theme.spacing.unit * 2.5,
    ]],
    maxWidth: 1010
  },
  logoImage: {
    width: theme.spacing.unit * 3.5,
    height: theme.spacing.unit * 3.5,
  },
  logoTitle: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  verticalDivider: {
    width: 1,
    margin: [[ 0, theme.spacing.unit * 2 ]],
    height: theme.spacing.unit * 4,
    transform: 'scaleX(0.5)',
    backgroundColor: '#262626',
    border: 'none',
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'inline-block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
});

class Header extends Component {

  state = {
    anchorEl: null,
    mobileMoreAnchorEl: null
  };

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
    this.handleMobileMenuClose();
  };

  handleMobileMenuOpen = event => {
    this.setState({ mobileMoreAnchorEl: event.currentTarget });
  };

  handleMobileMenuClose = () => {
    this.setState({ mobileMoreAnchorEl: null });
  };

  render(){
    const { anchorEl, mobileMoreAnchorEl } = this.state;
    const { classes } = this.props;
    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const renderMenu = (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMenuOpen}
        onClose={this.handleMenuClose}
      >
        <MenuItem onClick={this.handleMenuClose}>Profile</MenuItem>
        <MenuItem onClick={this.handleMenuClose}>My account</MenuItem>
      </Menu>
    );

    const renderMobileMenu = (
      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMobileMenuOpen}
        onClose={this.handleMobileMenuClose}
      >
        <MenuItem>
          <IconButton color="inherit">
            <Badge badgeContent={4} color="secondary">
              <ChatIcon />
            </Badge>
          </IconButton>
          <p>Messages</p>
        </MenuItem>
        <MenuItem>
          <IconButton color="inherit">
            <Badge badgeContent={11} color="secondary">
              <NotificationIcon />
            </Badge>
          </IconButton>
          <p>Notifications</p>
        </MenuItem>
        <MenuItem onClick={this.handleProfileMenuOpen}>
          <IconButton color="inherit">
            <AccountCircle />
          </IconButton>
          <p>Profile</p>
        </MenuItem>
      </Menu>
    );

  
    return(
      <>
        <AppBar position="sticky" color="default">
          <Toolbar className={classes.toolbarRoot}>
            <Grid container alignItems="center">
              <Grid item xs={true}>
                <Grid container alignItems="center">
                  <img alt="logo" src={logo} className={classes.logoImage} />
                  <hr className={classes.verticalDivider} />
                  <Typography variant="h6" className={classes.logoTitle}>
                    SharedIsBetter
                  </Typography>
                </Grid>
              </Grid>
  
              <Grid item xs={true} className={classes.search}>
                <div className={classes.searchIcon}>
                  <SearchIcon />
                </div>
                <InputBase
                  placeholder="Search..."
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput
                  }}
                />
              </Grid>
  
              <Grid item className={classes.sectionMobile}>
                <IconButton aria-haspopup="true" color="inherit" onClick={this.handleMobileMenuOpen}>
                  <MoreIcon />
                </IconButton>
              </Grid>
  
              <Grid item className={classes.sectionDesktop}>
                <IconButton>
                  <Badge badgeContent={4} color="primary">
                    <ChatIcon />
                  </Badge>
                </IconButton>
                <IconButton>
                  <Badge badgeContent={4} color="primary">
                    <NotificationIcon />
                  </Badge>
                </IconButton>
                  <IconButton color="inherit" onClick={this.handleProfileMenuOpen}>
                    <AccountCircle />
                  </IconButton>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
        {renderMenu}
        {renderMobileMenu}
      </>
    );
  }
};

export default withRoot(withStyles(styles)(Header));
