import React from 'react';

import { Typography } from '@material-ui/core';

import { components as LayoutComponents } from '../../layout';

const { MainLayout } = LayoutComponents;

const LandingPage = () => {
  return(
    <MainLayout>
      <Typography variant="h3">
        This is a landing page
      </Typography>
    </MainLayout>
  );
}

export default LandingPage;