import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { colors, CssBaseline } from '@material-ui/core';

const theme = createMuiTheme({
  palette: {
    primary: colors.teal,
    secondary: colors.pink,
  },
  // shadows: Array(25).fill('none'),
  overrides: {
    MuiAppBar: {
      root: {
        height: 77,
        alignItems: 'center',
        borderBottom: '1px solid rgba(0,0,0,0.0975)'
      },
      colorDefault: {
        backgroundColor: 'white'
      }
    },
    MuiIconButton: {
      root: {
        color: 'rgba(0, 0, 0, 0.87)'
      }
    }
  },
  typography: {
    useNextVariants: true,
  },
});

function withRoot(Component) {
  function WithRoot(props) {
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...props} />
      </MuiThemeProvider>
    )
  }
  return WithRoot;
}

export default withRoot;