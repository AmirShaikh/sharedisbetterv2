module.exports = {
  defaults: require('./defaults'),
  msgs: require('./msgs'),
  validations: require('./validations')
};