const passwordValidator = require('password-validator');
const _ = require('lodash');

const schema = new passwordValidator();
const config = {
  min: 6,
  max: 100
};
schema
  .is().min(config.min)
  .is().max(config.max)
  .has().uppercase()
  .has().lowercase()
  .has().digits()
  .has().symbols()
  .has().not().spaces();

const validate = password => schema.validate(password);
const validateWithList = password => schema.validate(password, { list: true });

const validatedPassword = password => {
  const list = validateWithList(password);
  let errors = {};

  if (_.includes(list, 'lowercase')) {
    errors.lowercase = 'have at least one lower case character';
  }
  if (_.includes(list, 'uppercase')) {
    errors.uppercase = 'have at least one capital character';
  }
  if (_.includes(list, 'digits')) {
    errors.digits = 'have at least one number';
  }
  if (_.includes(list, 'symbols')) {
    errors.symbols = 'have at least one symbol';
  }
  if (_.includes(list, 'lowercase')) {
    errors.spaces = 'not include space';
  }
  if (_.includes(list, 'min')) {
    errors.min = `be at least ${config.min} characters`;
  }
  if (_.includes(list, 'max')) {
    errors.max = `be max ${config.max} characters`;
  }

  return {
    errors,
    isValid: _.isEmpty(errors)
  };
}

module.exports = {
  schema,
  validate,
  validateWithList,
  validatedPassword
};