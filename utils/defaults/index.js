module.exports = {
  communityDefaults: {
    limit: "10",
    order: "-date"
  },
  postDefaults: {
    limit: "10",
    order: "-date"
  },
  userDefaults: {
    limit: "10",
    order: "-date"
  }
};