const app = require('express')();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');

// customs
const port = process.env.PORT || 5001;
const routes = require('./routes');
const { keys, passportConfig } = require('./config');

// bodyParser middleware
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(bodyParser.json());

// connect to db
mongoose
  .connect(keys.db, { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false })
  .then(() => console.info('DB connected'))
  .catch(error => console.error(error));

// passport middleware
app.use(passport.initialize());
passportConfig(passport);

// routes middleware
app.use('/api', routes);

app.listen(port, () => console.info(`Server running on port ${port}`));